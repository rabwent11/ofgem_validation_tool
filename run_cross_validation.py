import argparse
import datetime as dt
from cross_validation.customer_match_validation import validate_files

VERSION = 'v1.1.0'


def run():
    parser = argparse.ArgumentParser(description='Customer Match Validation - Version {}'.format(VERSION))

    # Input tariff file path
    parser.add_argument('-t', '--tariff-filepath', type=str, metavar='', required=False,
                        help='path to CSV tariff file to be verified',
                        # Please edit the line below, so that it has the path of the Tariff file to be validated.
                        default=r'C:\Users\username\cross_validation\input\tariff_data_correct.csv')
    # Input customer match file path
    parser.add_argument('-c', '--customer-filepath', type=str, metavar='', required=False,
                        help='path to CSV customer match file to be verified',
                        # Please edit the line below, so that it has the path of the Customer file to be validated.
                        default=r'C:\Users\username\cross_validation\input\customer_match_data_correct.csv')
    # Output directory
    parser.add_argument('-o', '--output-dirpath', type=str, metavar='', required=False,
                        help='path to directory where output files will be stored',
                        # Please edit the line below to indicate the directory where the output files will be saved.
                        default=r'C:\Users\username\cross_validation\output')

    ##
    # Optional parameters

    # Prefix for output files
    parser.add_argument('-p', '--prefix', type=str, metavar='', required=False,
                        help='supplier name or acronym to prefix output files',
                        # This is only used to tag filenames with a prefix (e.g., 'test_', 'supplierXYZ_')
                        default='')

    # Flag to match for supplier name
    # Since argparse's bool type is a bit unintuitive, we use a string type to emulate a boolean response here.
    parser.add_argument('-s', '--match-supplier', type=str, metavar='', required=False,
                        help='flag to match for supplier name in input files (default: True)',
                        # True to check that the supplier name matches both files (tariff and customer files).
                        # False to disregard the supplier name in case it has multiple marketing names in these files.
                        default='True')

    ##
    # Received arguments
    args = parser.parse_args()
    tariff_filepath = args.tariff_filepath
    customer_filepath = args.customer_filepath
    output_dirpath = args.output_dirpath
    prefix = args.prefix
    match_supplier = args.match_supplier

    # Prefix
    if prefix != '' and not prefix.endswith('_'):
        prefix += '_'

    # Match supplier - string to boolean response
    if match_supplier.lower() in ('true', 'yes', 't', 'y', '1'):
        match_supplier_flag = True
    else:
        # 'false', 'no', 'f', 'n', '0', etc
        match_supplier_flag = False

    # Timestamp for log and filenames
    str_now = dt.datetime.strftime(dt.datetime.now(), "%Y%m%d_%H%M%S")

    # Files that will be generated in the --output-dirpath provided
    output_filenames = {
        'elec_cust_count_diff': '{}elec_cust_count_diff_{}.csv'.format(prefix, str_now),
        'gas_cust_count_diff': '{}gas_cust_count_diff_{}.csv'.format(prefix, str_now),
        'elec_tariff_mismatch': '{}elec_tariff_mismatch_{}.csv'.format(prefix, str_now),
        'gas_tariff_mismatch': '{}gas_tariff_mismatch_{}.csv'.format(prefix, str_now)
    }

    return validate_files(tariff_filepath, customer_filepath, output_dirpath, output_filenames, match_supplier_flag)


if __name__ == '__main__':
    print('\nCustomer Match Validation - Version {}'.format(VERSION))
    if run():
        print('\n-----\nValidation check completed! -- Please see output files.')
    else:
        print('\n-----\nPlease see the error(s) above.')
    print('-----')
