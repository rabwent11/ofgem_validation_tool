from component_test import main_test
import os

import glob

import json


def js_r(filename):
    with open(filename) as f_in:
        return (json.load(f_in))


def test_read_file_data():
    output_dir = r'component_test'

    error_log_path = r'component_test'

    json_file = "unit_test_1.0.json"

    file_path = r"component_test\\*.csv"

    golden_data_set_path = r'local_validation_pipeline.component_test.golden_data_sets'
    validations = main_test.process_data(error_log_path, file_path, output_dir, json_file)

    golden_data_files = [os.path.join(golden_data_set_path, json_f) for json_f in glob.glob(golden_data_set_path) if json_f.endswith('.json')]
    generated_data_files = [json_f for json_f in validations if json_f.endswith('.json')]

    for expected_output, actual_output in zip(golden_data_files, generated_data_files):
        assert js_r(expected_output) == actual_output


if __name__ == '__main__':
    test_read_file_data()