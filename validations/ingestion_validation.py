from validations.data_pipeline import DataPipeline
import pandas as pd
import numpy as np


class IngestionValidation(DataPipeline):

    def calculate(self):
        ingestion_errors = []
        schema_check = [
            {"no_of_columns_check": self.no_of_columns_check()},
            {"missing_obligatory_columns": self.obligatory_columns()},
            {"missing_data_columns": self.miss_data_column_names()},
            {"missing_dict_columns": self.miss_dict_column_names()}]

        if self.miss_dict_column_names():

            invalid_cases = pd.DataFrame([i for i in self.miss_dict_column_names()])

            for index, row in invalid_cases.iterrows():
                error_df = pd.DataFrame({
                    'missing_column': row})

                ingestion_errors.append(error_df.to_json())
            print('ingestion check complete ')

        return ingestion_errors if ingestion_errors else None

    def sample_size_check(self):
        return len(self.df[self.row_id_column])

    def no_of_columns_check(self):
        columns_in_data = len(self.df.columns)
        return True if columns_in_data == self.no_of_columns else columns_in_data

    def miss_dict_column_names(self):
        return (set(self.schema_columns)) - (set(self.data_column_names))

    def miss_data_column_names(self):
        return (set(self.data_column_names)) - (set(self.schema_columns))

    def obligatory_columns(self):
        obligatory_columns = [i['to'] for i in self.schema_all if i['optional'] == False]
        return pd.DataFrame([{'missing_columns': np.array(set(obligatory_columns) - set(self.data_column_names))}])


if __name__ == "__main__":
    pass