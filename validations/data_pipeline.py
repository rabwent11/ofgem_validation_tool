from validations import collect
from validations import ingest
import datetime
import json
from pathlib import Path


class DataPipeline(collect.Collect):

        def __init__(self, error_log_path, output_dir, json_file, data_file):
            json_path = Path("jsons")/json_file
            now = datetime.datetime.now()
            self.timestamp = str(now.strftime("%Y%m%d_%H-%M-%S"))
            with open(json_path) as f:
                schema = json.load(f)

            self.schema_all = schema["columns"]
            self.conditional_validations = schema["conditional_validation"]
            self.schema_columns = {x["to"]: x for x in schema["columns"]}
            self.schema_dtypes = {x["type_to"]: x for x in schema["columns"]}
            self.date_columns = {x["to"]:x for x in schema["columns"] if x["type_to"]=='date' }
            self.date_columns = [i for i in self.date_columns]
            self.output_dir = output_dir
            self.error_log_path = error_log_path
            self.json_file = json_file
            self.file_path = data_file
            self.sample_size = 1
            self.no_of_columns = schema["no_of_columns"]

            self.df, self.supplier_name, self.file_name, self.file_type, self.no_of_customers = collect.Collect(self.file_path).csv_reader()

            self.df = ingest.Ingest(self.schema_all, self.df, self.supplier_name).calculate()

            self.df['row_index'] = self.df.index + 2  # +2 to match csv index - python indexing begins from 0
            self.row_id_column = self.df['row_index'].keys()
            self.total_rows = len(self.df)
            self.data_column_names = self.df.columns.str.lower().str.replace(' ', '_')


if __name__ == "__main__":
   pass

# Data_pipeline = DataPipeline(output_dir, json_file, file_path)