# -*- coding: utf-8 -*-
# This code is used to validate whether the number of customer accounts on each tariff ID between the "tariff" data set
# and the "customer match" data set are consistent.
# The reason for this is that the two data sets are joined together in order to calculate the number of dual fuel and
# single fuel customer accounts. If the number of customers or the tariff IDs/supplier names are not consistent between
# the two files, the final values will be incorrect and will not truthfully reflec the market

# Ofgem will not tolerate large mismatches between the two data sets. It is in the supplier's own interest to make sure
# the mismatch between the two data sets is kept to a minimum

import pandas as pd
from pathlib import Path


def validate_files(tariff_filepath, customer_filepath, output_dirpath, output_filenames, match_supplier):
    ok_filepaths = validate_filepaths(tariff_filepath, customer_filepath, output_dirpath)

    if not ok_filepaths:
        # Error messages already printed
        return False

    df_tariff = read_tariff_data(tariff_filepath)
    df_customer = read_customer_data(customer_filepath)

    # if match_supplier is True, then include the 'supplier_name' in all groups and joins.
    supplier_name = []
    if match_supplier:
        supplier_name = ['supplier_name']

    # Initial grouping
    grouper_1 = supplier_name + ['tariff_fuel_type', 'region', 'tariff_uid', 'payment_method', 'default_3_years']
    df_tariff = df_tariff.groupby(grouper_1, as_index=False).agg({'number_of_customer_accounts': 'sum'})

    # Split into electricity and gas
    df_tariff_elec = df_tariff.loc[df_tariff['tariff_fuel_type'] == 'E']
    df_tariff_gas = df_tariff.loc[df_tariff['tariff_fuel_type'] == 'G']

    print('\nChecking electricity tariffs and customers...')
    validate_electricity(df_tariff_elec, df_customer, output_dirpath, output_filenames, match_supplier)

    print('\nChecking gas tariffs and customers...')
    validate_gas(df_tariff_gas, df_customer, output_dirpath, output_filenames, match_supplier)

    # Validation check completed.
    return True


def validate_filepaths(tariff_filepath, customer_filepath, output_dirpath):
    # Pathlib understands empty strings as the current directory due to its Path constructor,
    # hence these paths must not be blank.
    result = True
    if not tariff_filepath.strip():
        print('\nERROR: Please provide a valid argument for --tariff-filepath.')
        result = False
    if not customer_filepath.strip():
        print('\nERROR: Please provide a valid argument for --customer-filepath.')
        result = False
    if not output_dirpath.strip():
        print('\nERROR: Please provide a valid argument for --output-dirpath.')
        result = False

    # Input files must exist and this script must have read access to them.
    if not Path(tariff_filepath).exists():
        print("\nERROR: Invalid path for Tariff file or this tool doesn't have read permissions at the path provided. "
              "Please check the argument --tariff-filepath provided.")
        result = False
    if not Path(customer_filepath).exists():
        print("\nERROR: Invalid path for Customer file or this tool doesn't have read permissions at the path provided."
              " Please check the argument --customer-filepath provided.")
        result = False

    # Output dirpath validation
    try:
        # Creates the output structure
        output_path = Path(output_dirpath)
        output_path.mkdir(parents=True, exist_ok=True)

    except PermissionError:
        print("\nERROR: Output directory is invalid or this tool doesn't have write permissions to it. "
              "Please check the argument --output-dir provided.")
        result = False

    # All validations passed
    return result


def read_tariff_data(tariff_filepath):
    # Define data types
    dtypes = {'supplier_name': 'str',
              'region': 'str',
              'tariff_uid': 'str',
              'payment_method': 'str',
              'default_3_years': 'str',
              'tariff_fuel_type': 'str',
              'number_of_customer_accounts': 'int'}

    # Read file into dataframe
    df = pd.read_csv(tariff_filepath, usecols=dtypes, dtype=dtypes)

    # Return df that is ready to work with
    return clean_dataframe(df, dtypes)


def read_customer_data(customer_filepath):
    # Define data types
    dtypes = {'supplier_name': 'str',
              'electricity_region': 'str',
              'gas_region': 'str',
              'number_accounts': 'int'}

    # Customer match file has a set of fields with suffixes _1 to _5.
    # They can be added explicitly to dictionary 'dtypes' or using a for loop.
    for suffix in range(1, 6):
        dtypes['electricity_tariff_uid_{}'.format(suffix)] = 'str'
        dtypes['gas_tariff_uid_{}'.format(suffix)] = 'str'
        dtypes['payment_method_electricity_{}'.format(suffix)] = 'str'
        dtypes['payment_method_gas_{}'.format(suffix)] = 'str'
        dtypes['default_3_years_electricity_{}'.format(suffix)] = 'str'
        dtypes['default_3_years_gas_{}'.format(suffix)] = 'str'

    # Read file into dataframe
    df = pd.read_csv(customer_filepath, usecols=dtypes, dtype=dtypes)

    # Return df that is ready to work with
    return clean_dataframe(df, dtypes)


def clean_dataframe(df, dtypes):
    # NaN/Null/None treatment and other pre-processing
    # In this case, NaN values for strings will be blanks '' and 0s for numeric fields
    # In addition, heading and trailing whitespaces will be stripped from strings (e.g. 'hello ' will be 'hello')

    # Strings
    str_cols = [k for k, v in dtypes.items() if v == 'str']
    df[str_cols] = df[str_cols].fillna('').applymap(lambda x: x.strip())

    # Numeric (Ints in this case - one column each df, but used procedure below allows for more columns)
    num_cols = [k for k, v in dtypes.items() if v in ['int', 'float']]
    df[num_cols] = df[num_cols].fillna(0)

    # Removing rows with number_of_customer_accounts = 0.
    # These aren't expected to have any matching rows from the customer match dataset
    # index_drop_0s = df[df['number_of_customer_accounts'] == 0].index
    # df.drop(index_drop_0s, inplace=True)

    # Return treated df
    return df


def validate_electricity(df_tariff_elec, df_customer, output_dirpath, output_filenames, match_supplier):
    # If match_supplier is True, then the 'supplier_name' is included in all groups and joins.
    supplier_name = []
    if match_supplier:
        supplier_name = ['supplier_name']

    # Accumulator dataframe
    df_customer_elec = pd.DataFrame()
    for i in range(1, 6):
        grouper = supplier_name + ['electricity_region',
                                   'electricity_tariff_uid_{}'.format(i),
                                   'payment_method_electricity_{}'.format(i),
                                   'default_3_years_electricity_{}'.format(i)]
        # Temporary dataframe
        df = df_customer.groupby(grouper, as_index=False).agg({'number_accounts': 'sum'})

        # Rename columns
        df.rename(columns={'electricity_tariff_uid_{}'.format(i): 'electricity_tariff_uid_cust_match',
                           'payment_method_electricity_{}'.format(i): 'payment_method_electricity_cust_match',
                           'default_3_years_electricity_{}'.format(i): 'default_3_years_electricity_cust_match'},
                  inplace=True)

        # Accumulate to main df
        df_customer_elec = pd.concat([df_customer_elec, df])

    # Calculate the total number of customer accounts per unique tariff combination
    grouper = supplier_name + ['electricity_region',
                               'electricity_tariff_uid_cust_match',
                               'payment_method_electricity_cust_match',
                               'default_3_years_electricity_cust_match']
    df_customer_elec = df_customer_elec.groupby(grouper, as_index=False).agg({'number_accounts': 'sum'})

    # Drop rows which have no electricity tariff uid
    invalid_tariff_uid = df_customer_elec[df_customer_elec['electricity_tariff_uid_cust_match'] == ''].index
    df_customer_elec.drop(invalid_tariff_uid, inplace=True)

    ##
    # Electricty - Matching checks
    # Columns from df_tariff_elec
    left_cols_join = supplier_name + ['region', 'tariff_uid', 'payment_method', 'default_3_years']
    # Columns from df_consumer_elec
    right_cols_join = supplier_name + ['electricity_region',
                                       'electricity_tariff_uid_cust_match',
                                       'payment_method_electricity_cust_match',
                                       'default_3_years_electricity_cust_match']
    df_joined_elec = df_tariff_elec.merge(df_customer_elec, how='outer', left_on=left_cols_join,
                                          right_on=right_cols_join)

    # fillna(0) is used again as these fields are results from group-by operations where resulting NaNs are possible
    df_joined_elec['difference'] = df_joined_elec['number_of_customer_accounts'].fillna(0) - \
                                   df_joined_elec['number_accounts'].fillna(0)

    #len(df_joined_elec[df_joined_elec['difference'] != 0])

    # Rows present in df_tariff_elec and not in df_consumer_elec
    df_right_missing_elec = df_joined_elec.loc[df_joined_elec['number_accounts'].isnull()]

    #cols = left_cols_join + ['difference']
    #print(len(df_right_missing_elec[cols]))
    #df_right_missing_elec[cols]

    # Rows present in df_consumer_elec and not in df_tariff_elec
    df_left_missing_elec = df_joined_elec.loc[df_joined_elec['number_of_customer_accounts'].isnull()]

    #cols = right_cols_join + ['difference']
    #print(len(df_left_missing_elec[cols]))
    #df_left_missing_elec[cols]

    # Electricity - in both and with a difference present
    # Present in both, but have a customer count difference
    # If this doesn't yield results and we know there are some differences in the dataframe,
    # it might mean that the difference is caused by unmatched rows
    df_elec_cust_difference = df_joined_elec.loc[(df_joined_elec['number_of_customer_accounts'].notnull())
                                                 & (df_joined_elec['number_accounts'].notnull())
                                                 & (df_joined_elec['difference'] != 0)]

    if 'elec_cust_count_diff' in output_filenames:
        csv_file_path = Path(output_dirpath) / output_filenames['elec_cust_count_diff']
        print('...saving to {}'.format(csv_file_path))
        df_elec_cust_difference.to_csv(csv_file_path, index=False, encoding='utf8')

    # Electricity - tariffs classifications are not matching between the two dataframes
    # This shows the tariffs in each dataframe which are not matching with a corresponding tariff in the other dataframe
    df_row_mismatch_elec = pd.concat([df_right_missing_elec, df_left_missing_elec])

    if 'elec_tariff_mismatch' in output_filenames:
        csv_file_path = Path(output_dirpath) / output_filenames['elec_tariff_mismatch']
        print('...saving to {}'.format(csv_file_path))
        df_row_mismatch_elec.to_csv(csv_file_path, index=False, encoding='utf8')


def validate_gas(df_tariff_gas, df_customer, output_dirpath, output_filenames, match_supplier):
    # If match_supplier is True, then the 'supplier_name' is included in all groups and joins.
    supplier_name = []
    if match_supplier:
        supplier_name = ['supplier_name']

    # Accumulator dataframe
    df_customer_gas = pd.DataFrame()
    for i in range(1, 6):
        grouper = supplier_name + ['gas_region',
                                   'gas_tariff_uid_{}'.format(i),
                                   'payment_method_gas_{}'.format(i),
                                   'default_3_years_gas_{}'.format(i)]
        # Temporary dataframe
        df = df_customer.groupby(grouper, as_index=False).agg({'number_accounts': 'sum'})

        # Rename columns
        df.rename(columns={'gas_tariff_uid_{}'.format(i): 'gas_tariff_uid_cust_match',
                           'payment_method_gas_{}'.format(i): 'payment_method_gas_cust_match',
                           'default_3_years_gas_{}'.format(i): 'default_3_years_gas_cust_match'},
                  inplace=True)

        # Accumulate to main df
        df_customer_gas = pd.concat([df_customer_gas, df])

    # Calculate the total number of customer accounts per unique tariff combination
    grouper = supplier_name + ['gas_region',
                               'gas_tariff_uid_cust_match',
                               'payment_method_gas_cust_match',
                               'default_3_years_gas_cust_match']
    df_customer_gas = df_customer_gas.groupby(grouper, as_index=False).agg({'number_accounts': 'sum'})

    # Drop rows which have no gas tariff uid
    invalid_tariff_uid = df_customer_gas[df_customer_gas['gas_tariff_uid_cust_match'] == ''].index
    df_customer_gas.drop(invalid_tariff_uid, inplace=True)

    ##
    # Gas - Matching checks
    left_cols_join = supplier_name + ['region', 'tariff_uid', 'payment_method', 'default_3_years']
    right_cols_join = supplier_name + ['gas_region', 'gas_tariff_uid_cust_match', 'payment_method_gas_cust_match',
                                       'default_3_years_gas_cust_match']
    df_joined_gas = df_tariff_gas.merge(df_customer_gas, how='outer', left_on=left_cols_join, right_on=right_cols_join)

    # fillna(0) is used again as these fields are results from group-by operations where resulting NaNs are possible
    df_joined_gas['difference'] = df_joined_gas['number_of_customer_accounts'].fillna(0) - \
                                  df_joined_gas['number_accounts'].fillna(0)

    #len(df_joined_gas[df_joined_gas['difference'] != 0])

    # Rows present in df_cm_val_1_gas and not in df_cm_val_2_gas
    df_right_missing_gas = df_joined_gas.loc[df_joined_gas['number_accounts'].isnull()]

    #cols = left_cols_join + ['difference']
    #print(len(df_right_missing_gas[cols]))
    #df_right_missing_gas[cols]

    # Rows present in df_cm_val_2_gas and not in df_cm_val_1_gas
    df_left_missing_gas = df_joined_gas.loc[df_joined_gas['number_of_customer_accounts'].isnull()]

    #cols = right_cols_join + ['difference']
    #print(len(df_left_missing_gas[cols]))
    #df_left_missing_gas[cols]

    # Gas - in both and with a difference present
    # Present in both, but have a customer count difference
    # If this doesn't yield results and we know there are some differences in the dataframe,
    # it might mean that the difference is caused by unmatched rows
    df_gas_cust_difference = df_joined_gas.loc[(df_joined_gas['number_of_customer_accounts'].notnull())
                                               & (df_joined_gas['number_accounts'].notnull())
                                               & (df_joined_gas['difference'] != 0)]

    if 'gas_cust_count_diff' in output_filenames:
        csv_file_path = Path(output_dirpath) / output_filenames['gas_cust_count_diff']
        print('...saving to {}'.format(csv_file_path))
        df_gas_cust_difference.to_csv(csv_file_path, index=False, encoding='utf8')

    # Gas - tariffs classifications are not matching between the two dataframes
    # This shows the tariffs in each dataframe which are not matching with a corresponding tariff in the other dataframe
    df_row_mismatch_gas = pd.concat([df_right_missing_gas, df_left_missing_gas])

    if 'gas_tariff_mismatch' in output_filenames:
        csv_file_path = Path(output_dirpath) / output_filenames['gas_tariff_mismatch']
        print('...saving to {}'.format(csv_file_path))
        df_row_mismatch_gas.to_csv(csv_file_path, index=False, encoding='utf8')
