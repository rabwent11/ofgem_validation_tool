# *** THIS SCRIPT IS NOT USED IN PRODUCTION ***
# The aim of this script is to carry out a few tests.

import pandas as pd
from pandas.util.testing import assert_frame_equal
from pathlib import Path
from cross_validation.customer_match_validation import validate_files


def run_tests():
    # Path to test cases
    input_dirpath = Path(r'C:\Users\username\cross_validation\input')
    output_dirpath = Path(r'C:\Users\username\cross_validation\output\test')
    output_dirpath.mkdir(parents=True, exist_ok=True)

    test_list = [
        {'id': 'test01', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_correct.csv'},

        {'id': 'test02', 'tariff_file': 'tariff_data_incorrect_default_3_years.csv', 'customer_file': 'customer_match_data_correct.csv'},
        {'id': 'test03', 'tariff_file': 'tariff_data_incorrect_name.csv', 'customer_file': 'customer_match_data_correct.csv'},
        {'id': 'test04', 'tariff_file': 'tariff_data_incorrect_number_of_customer_accounts.csv', 'customer_file': 'customer_match_data_correct.csv'},
        {'id': 'test05', 'tariff_file': 'tariff_data_incorrect_payment_method.csv', 'customer_file': 'customer_match_data_correct.csv'},
        {'id': 'test06', 'tariff_file': 'tariff_data_incorrect_region.csv', 'customer_file': 'customer_match_data_correct.csv'},
        {'id': 'test07', 'tariff_file': 'tariff_data_incorrect_tariff_uid.csv', 'customer_file': 'customer_match_data_correct.csv'},
        {'id': 'test08', 'tariff_file': 'tariff_data_missing rows.csv', 'customer_file': 'customer_match_data_correct.csv'},

        {'id': 'test09', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_electricity_default_3_years.csv'},
        {'id': 'test10', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_electricity_payment_method.csv'},
        {'id': 'test11', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_electricity_region.csv'},
        {'id': 'test12', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_electricity_tariff_uid.csv'},
        {'id': 'test13', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_gas_default_3_years.csv'},
        {'id': 'test14', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_gas_payment_method.csv'},
        {'id': 'test15', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_gas_region.csv'},
        {'id': 'test16', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_gas_tariff_uid.csv'},
        {'id': 'test17', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_name.csv'},
        {'id': 'test18', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_incorrect_number_of_customer_accounts.csv'},
        {'id': 'test19', 'tariff_file': 'tariff_data_correct.csv', 'customer_file': 'customer_match_data_missing_rows.csv'}
    ]

    for test in test_list:
        tariff_filepath = input_dirpath / test['tariff_file']
        customer_filepath = input_dirpath / test['customer_file']

        # Previous version - Output files
        elec_cust_count_filepath = output_dirpath / '{}_elec_cust_count_diff.csv'.format(test['id'])
        gas_cust_count_filepath = output_dirpath / '{}_gas_cust_count_diff.csv'.format(test['id'])
        elec_mismatch_filepath = output_dirpath / '{}_elec_tariff_mismatch.csv'.format(test['id'])
        gas_mismatch_filepath = output_dirpath / '{}_gas_tariff_mismatch.csv'.format(test['id'])

        # New version - Output files
        output_filenames = {
            'elec_cust_count_diff': '{}new_elec_cust_count_diff.csv'.format(test['id']),
            'gas_cust_count_diff': '{}new_gas_cust_count_diff.csv'.format(test['id']),
            'elec_tariff_mismatch': '{}new_elec_tariff_mismatch.csv'.format(test['id']),
            'gas_tariff_mismatch': '{}new_gas_tariff_mismatch.csv'.format(test['id'])
        }

        # Run previous version
        prev_version(tariff_filepath, customer_filepath, elec_cust_count_filepath, gas_cust_count_filepath,
                     elec_mismatch_filepath, gas_mismatch_filepath)

        # Run new version
        validate_files(str(tariff_filepath), str(customer_filepath), str(output_dirpath), output_filenames,
                       match_supplier=True)

        ##
        # Comparison

        # elec_cust_count_diff
        df_prev = pd.read_csv(elec_cust_count_filepath, header=0)
        df_new = pd.read_csv(output_dirpath / output_filenames['elec_cust_count_diff'], header=0)
        test['elec_cust_count_diff'] = 'OK' if are_dfs_equal(df_prev, df_new) else 'ERROR'

        # gas_cust_count_diff
        df_prev = pd.read_csv(gas_cust_count_filepath, header=0)
        df_new = pd.read_csv(output_dirpath / output_filenames['gas_cust_count_diff'], header=0)
        test['gas_cust_count_diff'] = 'OK' if are_dfs_equal(df_prev, df_new) else 'ERROR'

        # elec_tariff_mismatch
        df_prev = pd.read_csv(elec_mismatch_filepath, header=0)
        df_new = pd.read_csv(output_dirpath / output_filenames['elec_tariff_mismatch'], header=0)
        test['elec_tariff_mismatch'] = 'OK' if are_dfs_equal(df_prev, df_new) else 'ERROR'

        # gas_tariff_mismatch
        df_prev = pd.read_csv(gas_mismatch_filepath, header=0)
        df_new = pd.read_csv(output_dirpath / output_filenames['gas_tariff_mismatch'], header=0)
        test['gas_tariff_mismatch'] = 'OK' if are_dfs_equal(df_prev, df_new) else 'ERROR'

    # Comparison results altogether
    for test in test_list:
        print('\n{} - {} - {}'.format(test['id'], test['tariff_file'], test['customer_file']))
        print('...{} - elec_cust_count_diff'.format(test['elec_cust_count_diff']))
        print('...{} - gas_cust_count_diff'.format(test['gas_cust_count_diff']))
        print('...{} - elec_tariff_mismatch'.format(test['elec_tariff_mismatch']))
        print('...{} - gas_tariff_mismatch'.format(test['gas_tariff_mismatch']))


def are_dfs_equal(df1, df2):
    try:
        assert_frame_equal(df1, df2)
        return True
    except AssertionError:
        return False


def prev_version(tariff_filepath, customer_filepath, elec_cust_count_filepath, gas_cust_count_filepath,
                 elec_mismatch_filepath, gas_mismatch_filepath):
    # Import files
    CSV_TARIFF_FILE = tariff_filepath
    CSV_CUSTOMER_MATCH_FILE = customer_filepath

    # Output files
    # CSV_OUTPUT_FILE_X = r"C:\Users\..."
    # For each supplier, include the supplier name at the beginning of the file name e.g. outputs\supplier_name_elec_cust_count_difference.csv
    CSV_SUPPLIER_CHECK_ELEC_CUSTOMER_COUNT = elec_cust_count_filepath
    CSV_SUPPLIER_CHECK_GAS_CUSTOMER_COUNT = gas_cust_count_filepath
    CSV_SUPPLIER_CHECK_ELEC_ID_MISMATCH = elec_mismatch_filepath
    CSV_SUPPLIER_CHECK_GAS_ID_MISMATCH = gas_mismatch_filepath

    # -*- coding: utf-8 -*-
    """
    Created on Fri Feb 14 14:34:46 2020

    @author: Ofgem
    """
    # v1.1.0
    # This code is used to validate whether the number of customer accounts on each tariff ID between the "tariff" data set and the "customer match" data set are consistent.
    # The reason for this is that the two data sets are joined together in order to calculate the number of dual fuel and single fuel customer accounts. If the number of customers or the tariff IDs/supplier names are not consistent between the two files, the final values will be incorrect and will not truthfully reflec the market

    # Ofgem will not tolerate large mismatches between the two data sets. It is in the supplier's own interest to make sure the mismatch between the two data sets is kept to a minimum

    # import pandas as pd

    # Import files
    # CSV_TARIFF_FILE = r"C:\Users\...\test\Tariff_input.csv"
    # CSV_CUSTOMER_MATCH_FILE = r"C:\Users\...\test\Customer_input.csv"

    # Output files
    # CSV_OUTPUT_FILE_X = r"C:\Users\..."
    # For each supplier, include the supplier name at the beginning of the file name e.g. outputs\supplier_name_elec_cust_count_difference.csv
    # CSV_SUPPLIER_CHECK_ELEC_CUSTOMER_COUNT = r"C:\Users\...\test\elec_cust_count_difference.csv"
    # CSV_SUPPLIER_CHECK_GAS_CUSTOMER_COUNT = r"C:\Users\...\test\gas_cust_count_difference.csv"
    # CSV_SUPPLIER_CHECK_ELEC_ID_MISMATCH = r"C:\Users\...\test\elec_tariff_mismatch_list.csv"
    # CSV_SUPPLIER_CHECK_GAS_ID_MISMATCH = r"C:\Users\...\test\gas_tariff_mismatch_list.csv"

    # Define data types

    cm_val_1_dtypes = {'supplier_name': 'str',
                       'region': 'str',
                       'tariff_uid': 'str',
                       'payment_method': 'str',
                       'default_3_years': 'str',
                       'tariff_fuel_type': 'str',
                       'number_of_customer_accounts': 'int'}

    cm_val_2_dtypes = {'supplier_name': 'str',
                       'electricity_region': 'str',
                       'gas_region': 'str',
                       'number_accounts': 'int'}

    # Customer match file has a set of fields with suffixes _1 to _5. They can be added explicitly to dictionary 'cm_val_2_dtypes' or using a for loop.
    for suffix in range(1, 6):
        cm_val_2_dtypes['electricity_tariff_uid_{}'.format(suffix)] = 'str'
        cm_val_2_dtypes['gas_tariff_uid_{}'.format(suffix)] = 'str'
        cm_val_2_dtypes['payment_method_electricity_{}'.format(suffix)] = 'str'
        cm_val_2_dtypes['payment_method_gas_{}'.format(suffix)] = 'str'
        cm_val_2_dtypes['default_3_years_electricity_{}'.format(suffix)] = 'str'
        cm_val_2_dtypes['default_3_years_gas_{}'.format(suffix)] = 'str'

    # Read the input files which were imported
    df_cm_val_1 = pd.read_csv(CSV_TARIFF_FILE, usecols=cm_val_1_dtypes, dtype=cm_val_1_dtypes)
    df_cm_val_2 = pd.read_csv(CSV_CUSTOMER_MATCH_FILE, usecols=cm_val_2_dtypes, dtype=cm_val_2_dtypes)

    # Quick visualisation of dataframes and data types
    # Input the below commands into the console

    # df_cm_val_1.head()
    # df_cm_val_1.dtypes #note: strings are considered objects under Pandas

    # df_cm_val_2.head()
    # df_cm_val_2.dtypes

    # NaN/Null/None treatment and other pre-processing

    # In this case, NaN values for strings will be blanks '' and 0s for numeric fields
    # In addition, heading and trailing whitespaces will be stripped from strings (e.g. 'hello ' will be 'hello')

    # Strings
    str_cols_1 = [k for k, v in cm_val_1_dtypes.items() if v == 'str']
    str_cols_2 = [k for k, v in cm_val_2_dtypes.items() if v == 'str']

    df_cm_val_1[str_cols_1] = df_cm_val_1[str_cols_1].fillna('').applymap(lambda x: x.strip())
    df_cm_val_2[str_cols_2] = df_cm_val_2[str_cols_2].fillna('').applymap(lambda x: x.strip())

    # Numeric (Ints in this case - one column each df, but used procedure below allows for more columns)
    num_cols_1 = [k for k, v in cm_val_1_dtypes.items() if v in ['int', 'float']]
    num_cols_2 = [k for k, v in cm_val_2_dtypes.items() if v in ['int', 'float']]

    df_cm_val_1[num_cols_1] = df_cm_val_1[num_cols_1].fillna(0)
    df_cm_val_2[num_cols_2] = df_cm_val_2[num_cols_2].fillna(0)

    # Removing rows with number_of_customer_accounts = 0. These aren't expected to have any matching rows from the customer match dataset
    # indexDrop0T = df_cm_val_1[df_cm_val_1['number_of_customer_accounts'] == 0].index
    # df_cm_val_1.drop(indexDrop0T, inplace = True)

    # Groups and splits

    # Similar to the 'concat'field used in the previous version of the script
    grouper_1 = ['supplier_name', 'tariff_fuel_type', 'region', 'tariff_uid', 'payment_method', 'default_3_years']

    df_cm_val_1 = df_cm_val_1.groupby(grouper_1, as_index=False).agg({'number_of_customer_accounts': 'sum'})

    # The dataframe is split into two separate data frames, one for electricity tariffs and one for gas tariffs
    df_cm_val_1_elec = df_cm_val_1.loc[df_cm_val_1['tariff_fuel_type'] == 'E']
    df_cm_val_1_gas = df_cm_val_1.loc[df_cm_val_1['tariff_fuel_type'] == 'G']

    ## Customer match file - Electricity
    df_cm_val_2_elec = pd.DataFrame()

    grouper_2_1 = ['supplier_name',
                   'electricity_region',
                   'electricity_tariff_uid_1',
                   'payment_method_electricity_1',
                   'default_3_years_electricity_1']
    # Temporary dataframe
    df_cm_val_2_elec_1 = df_cm_val_2.groupby(grouper_2_1, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_elec_1.rename(columns={'electricity_tariff_uid_1': 'electricity_tariff_uid_cust_match',
                                       'payment_method_electricity_1': 'payment_method_electricity_cust_match',
                                       'default_3_years_electricity_1': 'default_3_years_electricity_cust_match'
                                       }, inplace=True)

    grouper_2_2 = ['supplier_name',
                   'electricity_region',
                   'electricity_tariff_uid_2',
                   'payment_method_electricity_2',
                   'default_3_years_electricity_2']
    # Temporary dataframe
    df_cm_val_2_elec_2 = df_cm_val_2.groupby(grouper_2_2, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_elec_2.rename(columns={'electricity_tariff_uid_2': 'electricity_tariff_uid_cust_match',
                                       'payment_method_electricity_2': 'payment_method_electricity_cust_match',
                                       'default_3_years_electricity_2': 'default_3_years_electricity_cust_match'
                                       }, inplace=True)

    grouper_2_3 = ['supplier_name',
                   'electricity_region',
                   'electricity_tariff_uid_3',
                   'payment_method_electricity_3',
                   'default_3_years_electricity_3']
    # Temporary dataframe
    df_cm_val_2_elec_3 = df_cm_val_2.groupby(grouper_2_3, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_elec_3.rename(columns={'electricity_tariff_uid_3': 'electricity_tariff_uid_cust_match',
                                       'payment_method_electricity_3': 'payment_method_electricity_cust_match',
                                       'default_3_years_electricity_3': 'default_3_years_electricity_cust_match'
                                       }, inplace=True)

    grouper_2_4 = ['supplier_name',
                   'electricity_region',
                   'electricity_tariff_uid_4',
                   'payment_method_electricity_4',
                   'default_3_years_electricity_4']
    # Temporary dataframe
    df_cm_val_2_elec_4 = df_cm_val_2.groupby(grouper_2_4, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_elec_4.rename(columns={'electricity_tariff_uid_4': 'electricity_tariff_uid_cust_match',
                                       'payment_method_electricity_4': 'payment_method_electricity_cust_match',
                                       'default_3_years_electricity_4': 'default_3_years_electricity_cust_match'
                                       }, inplace=True)

    grouper_2_5 = ['supplier_name',
                   'electricity_region',
                   'electricity_tariff_uid_5',
                   'payment_method_electricity_5',
                   'default_3_years_electricity_5']
    # Temporary dataframe
    df_cm_val_2_elec_5 = df_cm_val_2.groupby(grouper_2_5, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_elec_5.rename(columns={'electricity_tariff_uid_5': 'electricity_tariff_uid_cust_match',
                                       'payment_method_electricity_5': 'payment_method_electricity_cust_match',
                                       'default_3_years_electricity_5': 'default_3_years_electricity_cust_match'
                                       }, inplace=True)

    # Merge electricity files together
    df_cm_val_2_elec = pd.concat(
        [df_cm_val_2_elec_1, df_cm_val_2_elec_2, df_cm_val_2_elec_3, df_cm_val_2_elec_4, df_cm_val_2_elec_5])

    # Calculate the total number of customer accounts per unique tariff combination
    grouper_2 = ['supplier_name',
                 'electricity_region',
                 'electricity_tariff_uid_cust_match',
                 'payment_method_electricity_cust_match',
                 'default_3_years_electricity_cust_match']
    df_cm_val_2_elec = df_cm_val_2_elec.groupby(grouper_2, as_index=False).agg({'number_accounts': 'sum'})

    # Drop rows which have no electricity tariff uid
    indexBlanks_elec = df_cm_val_2_elec[df_cm_val_2_elec['electricity_tariff_uid_cust_match'] == ''].index
    df_cm_val_2_elec.drop(indexBlanks_elec, inplace=True)

    ## Customer match file - Gas
    df_cm_val_2_gas = pd.DataFrame()

    grouper_2_1 = ['supplier_name',
                   'gas_region',
                   'gas_tariff_uid_1',
                   'payment_method_gas_1',
                   'default_3_years_gas_1']
    # Temporary dataframe
    df_cm_val_2_gas_1 = df_cm_val_2.groupby(grouper_2_1, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_gas_1.rename(columns={'gas_tariff_uid_1': 'gas_tariff_uid_cust_match',
                                      'payment_method_gas_1': 'payment_method_gas_cust_match',
                                      'default_3_years_gas_1': 'default_3_years_gas_cust_match'
                                      }, inplace=True)

    grouper_2_2 = ['supplier_name',
                   'gas_region',
                   'gas_tariff_uid_2',
                   'payment_method_gas_2',
                   'default_3_years_gas_2']
    # Temporary dataframe
    df_cm_val_2_gas_2 = df_cm_val_2.groupby(grouper_2_2, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_gas_2.rename(columns={'gas_tariff_uid_2': 'gas_tariff_uid_cust_match',
                                      'payment_method_gas_2': 'payment_method_gas_cust_match',
                                      'default_3_years_gas_2': 'default_3_years_gas_cust_match'
                                      }, inplace=True)

    grouper_2_3 = ['supplier_name',
                   'gas_region',
                   'gas_tariff_uid_3',
                   'payment_method_gas_3',
                   'default_3_years_gas_3']
    # Temporary dataframe
    df_cm_val_2_gas_3 = df_cm_val_2.groupby(grouper_2_3, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_gas_3.rename(columns={'gas_tariff_uid_3': 'gas_tariff_uid_cust_match',
                                      'payment_method_gas_3': 'payment_method_gas_cust_match',
                                      'default_3_years_gas_3': 'default_3_years_gas_cust_match'
                                      }, inplace=True)

    grouper_2_4 = ['supplier_name',
                   'gas_region',
                   'gas_tariff_uid_4',
                   'payment_method_gas_4',
                   'default_3_years_gas_4']
    # Temporary dataframe
    df_cm_val_2_gas_4 = df_cm_val_2.groupby(grouper_2_4, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_gas_4.rename(columns={'gas_tariff_uid_4': 'gas_tariff_uid_cust_match',
                                      'payment_method_gas_4': 'payment_method_gas_cust_match',
                                      'default_3_years_gas_4': 'default_3_years_gas_cust_match'
                                      }, inplace=True)

    grouper_2_5 = ['supplier_name',
                   'gas_region',
                   'gas_tariff_uid_5',
                   'payment_method_gas_5',
                   'default_3_years_gas_5']
    # Temporary dataframe
    df_cm_val_2_gas_5 = df_cm_val_2.groupby(grouper_2_5, as_index=False).agg({'number_accounts': 'sum'})
    # Rename columns
    df_cm_val_2_gas_5.rename(columns={'gas_tariff_uid_5': 'gas_tariff_uid_cust_match',
                                      'payment_method_gas_5': 'payment_method_gas_cust_match',
                                      'default_3_years_gas_5': 'default_3_years_gas_cust_match'
                                      }, inplace=True)

    # Merge gas files together
    df_cm_val_2_gas = pd.concat(
        [df_cm_val_2_gas_1, df_cm_val_2_gas_2, df_cm_val_2_gas_3, df_cm_val_2_gas_4, df_cm_val_2_gas_5])

    # Calculate the total number of customer accounts per unique tariff combination
    grouper_2 = ['supplier_name',
                 'gas_region',
                 'gas_tariff_uid_cust_match',
                 'payment_method_gas_cust_match',
                 'default_3_years_gas_cust_match']
    df_cm_val_2_gas = df_cm_val_2_gas.groupby(grouper_2, as_index=False).agg({'number_accounts': 'sum'})

    # Drop rows which have no gas tariff uid
    indexBlanks_gas = df_cm_val_2_gas[df_cm_val_2_gas['gas_tariff_uid_cust_match'] == ''].index
    df_cm_val_2_gas.drop(indexBlanks_gas, inplace=True)

    ## Matching

    ## Electricty
    left_cols_join = ['supplier_name', 'region', 'tariff_uid', 'payment_method', 'default_3_years']
    right_cols_join = ['supplier_name', 'electricity_region', 'electricity_tariff_uid_cust_match',
                       'payment_method_electricity_cust_match', 'default_3_years_electricity_cust_match']
    df_joined_elec = df_cm_val_1_elec.merge(df_cm_val_2_elec, how='outer', left_on=left_cols_join,
                                            right_on=right_cols_join)

    # fillna(0) is used again as these fields are results from group-by operations where resulting NaN values are possible
    df_joined_elec['difference'] = df_joined_elec['number_of_customer_accounts'].fillna(0) - df_joined_elec[
        'number_accounts'].fillna(0)

    len(df_joined_elec[df_joined_elec['difference'] != 0])

    # Rows present in df_cm_val_1_elec and not in df_cm_val_2_elec
    df_right_missing_elec = df_joined_elec.loc[df_joined_elec['number_accounts'].isnull()]

    cols = left_cols_join + ['difference']
    print(len(df_right_missing_elec[cols]))
    df_right_missing_elec[cols]

    # Rows present in df_cm_val_2_elec and not in df_cm_val_1_elec
    df_left_missing_elec = df_joined_elec.loc[df_joined_elec['number_of_customer_accounts'].isnull()]

    cols = right_cols_join + ['difference']
    print(len(df_left_missing_elec[cols]))
    df_left_missing_elec[cols]

    # Electricity - in both and with a difference present
    # Present in both, but have a customer count difference
    # If this doesn't yield results and we know there are some differences in the dataframe, it might mean that the difference is caused by unmatched rows
    df_elec_cust_difference = df_joined_elec.loc[(df_joined_elec['number_of_customer_accounts'].notnull())
                                                 & (df_joined_elec['number_accounts'].notnull())
                                                 & (df_joined_elec['difference'] != 0)]

    df_elec_cust_difference.to_csv(CSV_SUPPLIER_CHECK_ELEC_CUSTOMER_COUNT, index=False, encoding='utf8')

    # Electricity - tariffs classifications are not matching between the two dataframes
    # This shows the tariffs in each dataframe which are not matching with a corresponding tariff in the other dataframe
    df_row_mismatch_elec = pd.concat([df_right_missing_elec, df_left_missing_elec])

    df_row_mismatch_elec.to_csv(CSV_SUPPLIER_CHECK_ELEC_ID_MISMATCH, index=False, encoding='utf8')

    ## Gas
    left_cols_join = ['supplier_name', 'region', 'tariff_uid', 'payment_method', 'default_3_years']
    right_cols_join = ['supplier_name', 'gas_region', 'gas_tariff_uid_cust_match', 'payment_method_gas_cust_match',
                       'default_3_years_gas_cust_match']
    df_joined_gas = df_cm_val_1_gas.merge(df_cm_val_2_gas, how='outer', left_on=left_cols_join,
                                          right_on=right_cols_join)

    # fillna(0) is used again as these fields are results from group-by operations where resulting NaN values are possible
    df_joined_gas['difference'] = df_joined_gas['number_of_customer_accounts'].fillna(0) - df_joined_gas[
        'number_accounts'].fillna(0)

    len(df_joined_gas[df_joined_gas['difference'] != 0])

    # Rows present in df_cm_val_1_gas and not in df_cm_val_2_gas
    df_right_missing_gas = df_joined_gas.loc[df_joined_gas['number_accounts'].isnull()]

    cols = left_cols_join + ['difference']
    print(len(df_right_missing_gas[cols]))
    df_right_missing_gas[cols]

    # Rows present in df_cm_val_2_gas and not in df_cm_val_1_gas
    df_left_missing_gas = df_joined_gas.loc[df_joined_gas['number_of_customer_accounts'].isnull()]

    cols = right_cols_join + ['difference']
    print(len(df_left_missing_gas[cols]))
    df_left_missing_gas[cols]

    # Gas - in both and with a difference present
    # Present in both, but have a customer count difference
    # If this doesn't yield results and we know there are some differences in the dataframe, it might mean that the difference is caused by unmatched rows
    df_gas_cust_difference = df_joined_gas.loc[(df_joined_gas['number_of_customer_accounts'].notnull())
                                               & (df_joined_gas['number_accounts'].notnull())
                                               & (df_joined_gas['difference'] != 0)]

    df_gas_cust_difference.to_csv(CSV_SUPPLIER_CHECK_GAS_CUSTOMER_COUNT, index=False, encoding='utf8')

    # Gas - tariffs classifications are not matching between the two dataframes
    # This shows the tariffs in each dataframe which are not matching with a corresponding tariff in the other dataframe
    df_row_mismatch_gas = pd.concat([df_right_missing_gas, df_left_missing_gas])

    df_row_mismatch_gas.to_csv(CSV_SUPPLIER_CHECK_GAS_ID_MISMATCH, index=False, encoding='utf8')


if __name__ == '__main__':
    run_tests()
