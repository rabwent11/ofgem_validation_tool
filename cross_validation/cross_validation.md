# Cross Validation
## Table of Contents

1. [Customer Match Validation](#validation)
2. [Instructions](#instructions)
3. [Input Files](#input_files)
    1. [Tariff Data File Structure](#tariff_file_structure)
    2. [Customer Match Data File Structure](#customer_file_structure)
4. [Output Files](#output_files)
    1. [Electricity Customer Count Difference](#elec_cust_count_diff)
    2. [Gas Customer Count Difference](#gas_cust_count_diff)
    3. [Electricity Tariff Mismatch List](#elec_tariff_mismatch)
    4. [Gas Tariff Mismatch List](#gas_tariff_mismatch)

## <a name='validation'></a>Customer Match Validation
This validation process checks that a supplier's tariff data for electricity and gas are consistent with their customer accounts in each operating UK region.

## <a name='instructions'></a>Instructions
The Python script that initiates this validation process is `run_cross_validation.py`. The following subsections deal with the necessarity preconditions and arguments to run this script, including a couple of options on how to run it

### Preconditions:
1. Input files of tariff and customer data with their corresponding structure (please see [Input Files](#input_files)).

2. Python environment with installed required libraries (see `requirements.txt`).
    
    2.1 Create a new Python environment using your preferred approach (e.g., 
    `python -m venv /path/to/new/virtual/environment`, `virtualenvwrapper`, `pyenv`, `conda`, etc).
    
    2.2 Install required libraries (e.g., using `pip`: `pip install -r requirements.txt`).
    
    2.3 Activate this Python environment.

### Arguments:
* `-t`, `--tariff-filepath`: (**required**) path to CSV tariff file to be verified, e.g. `C:\Users\username\cross_validation\input\tariff_data.csv`.
* `-c`, `--customer-filepath`: (**required**) path to CSV customer match file to be verified, e.g. `C:\Users\username\cross_validation\input\customer_match_data.csv`.
* `-o`, `--output-dirpath`: (**required**) path to directory where output files will be stored, e.g. `C:\Users\username\cross_validation\output`.
* `-p`, `--prefix`: (**optional**) supplier name or acronym to prefix output files, e.g. `supplierXYZ_`, `test_`.
* `-s`, `--match-supplier`: (**optional**) flag to match for supplier name in input files (default: `True`), e.g. `True`, `False`.
That is, if this argument is `True`, the process will check that the supplier name matches both files (tariff and customer files). 
On the other hand, if the supplier has multiple marketing names, this argument can be set to `False` to bypass this check and avoid unintended mismatches.


### Run:
* **Option 1**: edit `run_cross_validation.py` and update function `run()`, almost at the top, with the desired parameters and run it (e.g., `python run_cross_validation.py`).
* **Option 2**: run it from the command line with the desired parameters. E.g.,
`python run_cross_validation.py -t C:\Users\username\cross_validation\input\tariff_data.csv -c C:\Users\username\cross_validation\input\customer_match_data.csv -o C:\Users\username\cross_validation\output` 

### Output:
The four output files will be stored in the `--output-dirpath` provided. These output files include a timestamp in their name, so that they are not replaced when the cross validation script is re-run.


## <a name='input_files'></a>Input Files
### <a name='tariff_file_structure'></a>Tariff Data File Structure
The tariff data file contains the following columns - please cross-check with the most recent json in the metadata project:
1.	supplier_name
2.	date
3.	tariff_uid
4.	tariff_advertised_name
5.	region
6.	meter_type
7.	tariff_type
8.	tariff_fuel_type
9.	payment_method
10.	online_account
11.	paperless_billing
12.	renewable_percentage
13.	default_3_years
14. default_customers_moved
15.	number_of_customer_accounts
16. is_tou_tariff
17. is_ev_tariff
18.	is_multi_reg_tariff
19.	is_multi_tier_tariff
20.	standing_charge
21.	single_rate_unit_rate
22.	multi_tier_volume_break_1
23.	multi_tier_volume_break_2
24.	multi_tier_volume_break_3
25.	multi_tier_volume_break_4
26.	multi_tier_volume_break_1_uom
27.	multi_tier_volume_break_2_uom
28.	multi_tier_volume_break_3_uom
29.	multi_tier_volume_break_4_uom
30.	multi_tier_unit_rate_1
31.	multi_tier_unit_rate_2
32.	multi_tier_unit_rate_3
33.	multi_tier_unit_rate_4
34.	multi_tier_unit_rate_5
35.	multi_tier_unit_rate_op
36.	multi_tier_unit_rate_op_2
37.	multi_tier_unit_rate_op_3
38.	assumed_consumption_split_1
39.	assumed_consumption_split_2
40.	assumed_consumption_split_3
41.	assumed_consumption_split_4
42.	assumed_consumption_split_5
43.	multi_reg_period_1_unit_rate
44.	multi_reg_period_2_unit_rate
45.	multi_reg_period_3_unit_rate
46.	multi_reg_period_4_unit_rate
47.	multi_reg_period_5_unit_rate
48.	dual_fuel_discount
49.	online_discount
50.	termination_fee
51.	fix_length
52.	tariff_offer_date
53.	tariff_withdraw_date
54.	tariff_expiry_date
55.	tariff_change_date

### <a name='customer_file_structure'></a>Customer Match Data File Structure
The customer data file contains the following columns - please cross-check with the most recent json in the metadata project:
1.	supplier_name
2.	date
3.	gas_tariff_uid_1
4.	gas_tariff_uid_2
5.	gas_tariff_uid_3
6.	gas_tariff_uid_4
7.	gas_tariff_uid_5
8.	electricity_tariff_uid_1
9.	electricity_tariff_uid_2
10.	electricity_tariff_uid_3
11.	electricity_tariff_uid_4
12.	electricity_tariff_uid_5
13.	gas_region
14.	electricity_region
15.	payment_method_gas_1
16.	payment_method_gas_2
17.	payment_method_gas_3
18.	payment_method_gas_4
19.	payment_method_gas_5
20.	payment_method_electricity_1
21.	payment_method_electricity_2
22.	payment_method_electricity_3
23.	payment_method_electricity_4
24.	payment_method_electricity_5
25.	default_3_years_gas_1
26.	default_3_years_gas_2
27.	default_3_years_gas_3
28.	default_3_years_gas_4
29.	default_3_years_gas_5
30.	default_3_years_electricity_1
31.	default_3_years_electricity_2
32.	default_3_years_electricity_3
33.	default_3_years_electricity_4
34.	default_3_years_electricity_5
35.	number_accounts

## <a name='output_files'></a>Output Files
These resulting files will highlight problematic rows from the input files if there is any important mismatch. 
Also, these files will only have the header row if the validation process did not identify issues with the input files.

### <a name='elec_cust_count_diff'></a>Electricity Customer Count Difference
This file contains the following columns:
1.	supplier_name
2.	tariff_fuel_type
3.	region
4.	tariff_uid
5.	payment_method
6.	default_3_years
7.	number_of_customer_accounts
8.	electricity_region
9.	electricity_tariff_uid_cust_match
10.	payment_method_electricity_cust_match
11.	default_3_years_electricity_cust_match
12.	number_accounts
13.	difference

### <a name='gas_cust_count_diff'></a>Gas Customer Count Difference
This file contains the following columns:
1.	supplier_name
2.	tariff_fuel_type
3.	region
4.	tariff_uid
5.	payment_method
6.	default_3_years
7.	number_of_customer_accounts
8.	gas_region
9.	gas_tariff_uid_cust_match
10.	payment_method_gas_cust_match
11.	default_3_years_gas_cust_match
12.	number_accounts
13.	difference

### <a name='elec_tariff_mismatch'></a>Electricity Tariff Mismatch List
This file contains the following columns:
1.	supplier_name
2.	tariff_fuel_type
3.	region
4.	tariff_uid
5.	payment_method
6.	default_3_years
7.	number_of_customer_accounts
8.	electricity_region
9.	electricity_tariff_uid_cust_match
10.	payment_method_electricity_cust_match
11.	default_3_years_electricity_cust_match
12.	number_accounts
13.	difference

### <a name='gas_tariff_mismatch'></a>Gas Tariff Mismatch List
This file contains the following columns:
1.	supplier_name
2.	tariff_fuel_type
3.	region
4.	tariff_uid
5.	payment_method
6.	default_3_years
7.	number_of_customer_accounts
8.	gas_region
9.	gas_tariff_uid_cust_match
10.	payment_method_gas_cust_match
11.	default_3_years_gas_cust_match
12.	number_accounts
13.	difference
